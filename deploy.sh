#!/bin/bash

# Stop and remove the existing container if it exists
docker stop forum || true
docker rm forum || true

# Pull the latest image
docker pull cowbuno/forum:latest

# Run the new container
docker run -d --name forum -p 80:80 cowbuno/forum:latest

# Clean up unused images
docker image prune -f
